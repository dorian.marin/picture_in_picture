import 'package:flutter/material.dart';
import 'package:video_player/video_player.dart';
import 'package:flutter/services.dart';

// https://flutter.github.io/assets-for-api-docs/assets/videos/butterfly.mp4
// https://flutter.github.io/assets-for-api-docs/assets/videos/bee.mp4
// https://www.youtube.com/watch?v=IhO3Y1unYGE&ab_channel=MundoCanticuentos.mp4
// https://www.youtube.com/watch?v=IhO3Y1unYGE.mp4

class PipPlayer extends StatefulWidget {
  const PipPlayer({super.key});

  @override
  State<PipPlayer> createState() => _PipPlayerState();
}

class _PipPlayerState extends State<PipPlayer> {
  late VideoPlayerController _controller;
  final String _myVideo =
      'https://flutter.github.io/assets-for-api-docs/assets/videos/butterfly.mp4';

  @override
  void initState() {
    super.initState();
    _controller = VideoPlayerController.network(_myVideo)
      ..initialize().then((_) => {
            setState(() {}),
          });
  }

  @override
  void dispose() {
    _controller.dispose();
    super.dispose();
  }

  static const platform = MethodChannel('flutter.ordendelfenix.com.channel');

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text("Picture in picture DEMO"),
      ),
      floatingActionButton: Row(
        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
        children: [
          FloatingActionButton(
              child: Icon(
                  _controller.value.isPlaying ? Icons.pause : Icons.play_arrow),
              onPressed: () {
                setState(() {
                  _controller.value.isPlaying
                      ? _controller.pause()
                      : _controller.play();
                });
              }),
          FloatingActionButton(
            child: const Icon(Icons.picture_in_picture_alt),
            onPressed: () async {
              await platform.invokeMethod('showNativeView');
            },
          )
        ],
      ),
      body: Center(
        child: SizedBox(
          width: double.infinity,
          height: 350,
          child: _controller.value.isInitialized
              ? VideoPlayer(_controller)
              : Container(),
        ),
      ),
    );
  }
}
